# VCardBuilder

Simple VCardBuilder. With the help of this class you are easyly able to create VCard-Files in php!.

## Version

> v1.0.0

### The versioning undergoes following rules

first number represents the `major` version

second number represents the `minor` version

last number represents the `build` version

For example v1.4.512 means: Its the 512th build for the version 1.4 and the 4 basically means its the 4th change of the major version 1.0.0

---

## How to create a vCard?

First you create a object of the VCardBuilder. Then you'll add all your information. At last you call the `build` function where you specify the path and the files name

Example:

```php
$vcard = new VCardBuilder();
$vcard->addName($lastName, $firstName);
$vcard->build($path);
// with $path you can define where the file is going to be safed and how it is named.
```

---

## Integrate vCardBuilder with GravityForms

This will temporarily create a .vcf File with gets attached to the email which the administrator recieves. After that the file immediately gets deleted. (If you want to avoid it leave out the `delete_vcard() function and add_action('gform_after_submission_1', 'delete_vcard', 10, 2)`)

To achieve this result add the following to the `functions.php`

```php
function add_vcard($notification, $form, $entry)
{
    if ($notification['name'] == 'Admin Notification') {

        $vcard = new VCardBuilder();

        $firstname = rgar($entry, '1.3');
        $lastname = rgar($entry, '1.6');

        $vcard->addName($lastname, $firstname);

        $vcard->addPhoneNumber(rgar($entry, '7'));
        $vcard->addEmail(rgar($entry, '2'));

        $upload = wp_upload_dir();
        $upload_path = $upload['basedir'];

        $firstname = preg_replace("/[^a-zA-Z0-9]/", "", $firstname);
        $lastname = preg_replace("/[^a-zA-Z0-9]/", "", $lastname);
        $filename = $lastname . '-' . $firstname;
        $filename = strtolower($filename);

        $vcard->build($upload_path . '/' . $filename . '.vcf');

        $attachment = $upload_path . '/' . $filename . '.vcf';

        if (file_exists($attachment)) {
            $notification['attachments']   = rgar($notification, 'attachments', array());
            $notification['attachments'][] = $attachment;
        }
    }


    return $notification;
}
add_filter('gform_notification_1', 'add_vcard', 10, 3);
```

```php
function delete_vcard($entry, $form)
{
    $upload = wp_upload_dir();
    $upload_path = $upload['basedir'];

    $firstname = preg_replace("/[^a-zA-Z0-9]/", "", rgar($entry, '1.3'));
    $lastname = preg_replace("/[^a-zA-Z0-9]/", "", rgar($entry, '1.6'));
    $filename = $lastname . '-' . $firstname . '.vcf';
    $filename = strtolower($filename);

    $vcard = $upload_path . '/' . $filename;

    if (file_exists($vcard)) {
        unlink($vcard);
    }
}
add_action('gform_after_submission_1', 'delete_vcard', 10, 2);
```

If you have multiple forms you simply copy this function and edit it to your needs. At last update`gform_notification_1` or `gform_after_submission_1`, all you have to do is replace the number and adjust it to your ***form_id***

With the `rgar($entry, {$id})` function you simply get the data out of a specific field. You can access its field by its id, where the first number indicates the ***form_id*** and the following numbers the field id itself.
