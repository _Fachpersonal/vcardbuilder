<?php 
$toolJSON = json_decode(file_get_contents(__DIR__ . "/../../tool.json"),true);
?>
<footer>
    <div class="left">
        <p><?=$toolJSON['toolDescription']?></p>
    </div>
    <div class="right">
        <ul>
            <li><a href="<?=$toolJSON['toolRepo']?>">Gitlab-Repository</a></li>
            <li><a href="<?=$toolJSON['author_URL']?>">Link to creator</a></li>
            <!-- <li><a href="<?=CONTACT_ME?>">Contact me</a></li> -->
        </ul>
    </div>
</footer>