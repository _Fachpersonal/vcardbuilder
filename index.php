<?php 
$toolJSON = json_decode(file_get_contents(__DIR__ . "/tool.json"),true);
?>
<html>
    <head>
        <title>VCardBuilder</title>
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css" />
    </head>
    <body>
        <?php require_once(__DIR__ . "/assets/parts/header.php")?>
        <article>
            <h3>Tool Info</h3>
            <hr>
            <div style="display:flex">
                <h6 style="float:left;padding-right:1em;margin:0">Author</h6>
                <a style="float:right" target="_blank" href="<?= $toolJSON['author_URL'] ?>"><?= $toolJSON['author'] ?></a>
            </div>
            <br>
            <div style="display:flex">
                <h6 style="float:left;padding-right:1em;margin:0">Description</h6>
                <p style="float:right;margin:0"><?= $toolJSON['toolDescription'] ?></a>
            </div>
            <br>
            <div style="display:flex">
                <h6 style="float:left;padding-right:1em;margin:0">Repository</h6>
                <a style="float:right" target="_blank" href="<?= $toolJSON['toolRepo'] ?>">Repository</a>
            </div>
        </article>
        <?php require_once(__DIR__ . "/assets/parts/footer.php")?>
    </body>
</html>