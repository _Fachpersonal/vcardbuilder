<?php

class VCardBuilder {
    
    private string $adr;
    private string $email;
    private string $n;
    private string $org;
    private string $tel;
    private string $title;
    private string $url;
    
    function addAddress(string $street, string $city, int $zip, string $country = "", string $type = "home") {
        $this->adr = (isset($this->adr) ? $this->adr."\n":""). sprintf("ADR;CHARSET=UTF-8;%s;type=pref:;;%s;%s;;%s;%s", strtoupper($type), $street, $city, $zip, $country);
    }
    
    function addEmail(string $email, string $type = "HOME") {
        $this->email = (isset($this->email) ? $this->email."\n":""). sprintf("EMAIL;TYPE=%s:%s",strtoupper($type),$email);
    }

    function addName(string $lastName, string $firstName, string $title="", string $displayName="") {
        $this->n = sprintf("N;CHARSET=UTF-8:%s;%s;;%s;%s;\nFN;CHARSET=UTF-8:%s %s %s",$lastName,$firstName,$title,$displayName, $title, $firstName, $lastName);
    }

    function addOrganisation(string $organisationName, string $organisationUnit) {
        $this->org = sprintf("ORG;CHARSET=UTF-8:%s;%s", $organisationName, $organisationUnit);
    }

    function addPhoneNumber(string $number, string $type="HOME") {
        $this->tel = (isset($this->tel)?$this->tel . "\n":""). sprintf("TEL;TYPE=%s;VOICE:%s",strtoupper($type), $number);
    }

    function addTitle(string $title) {
        $this->title = sprintf("TITLE;CHARSET=UTF-8:",$title);
    }

    function addURL(string $url, string $type="PRIVATE") {
        $this->url = (isset($this->url)?$this->url . "\n":""). sprintf("URL;type=%s:%s",strtoupper($type),$url);
    }


    function build(string $path) {
        $content = "BEGIN:VCARD\nVERSION:4.0\n";
        if(isset($this->n)){
            $content .= $this->n . "\n";
        }
        if(isset($this->adr)){
            $content .= $this->adr . "\n";
        }
        if(isset($this->email)){
            $content .= $this->email . "\n";
        }
        if(isset($this->org)){
            $content .= $this->org . "\n";
        }
        if(isset($this->tel)){
            $content .= $this->tel . "\n";
        }
        if(isset($this->title)){
            $content .= $this->title . "\n";
        }
        if(isset($this->url)){
            $content .= $this->url . "\n";
        }
        $content .= "END:VCARD";
        $file = fopen($path,"w");
        fwrite($file, $content);
    }
}
?>